﻿using System;
using System.Configuration;
using Boromak.Snippets.Testing.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkIt.Commands;

namespace ParkIt.Tests
{
    [TestClass]
    public class CommandParserTests
    {
        [TestMethod]
        public void CommandParserGeneratesCommandFromString()
        {
            ParkingManager pk = new ParkingManager();
            var command = CommandParser.ParseLineToCommand(pk, "display ALL");
            Assert.IsInstanceOfType(command, typeof(DisplayCommand));
        }

        [TestMethod]
        public void CommandParserGeneratedDisplayCommandDisplaysPlacesLeft()
        {
            ParkingManager pk = new ParkingManager();
            var command = CommandParser.ParseLineToCommand(pk, "display RED");
            using (var consoleOut = new ConsoleOutput())
            {
                command.ExecuteCommand();
                Assert.AreEqual("Parking lot RED has 20 places left.\r\n", consoleOut.GetOuput());
            }
        }

        [TestMethod]
        public void CommandParserFailsOnUnknownCommand()
        {
            ParkingManager pk = new ParkingManager();
            var command = CommandParser.ParseLineToCommand(pk, "JiggleJiggleJiggleJiggleJiggle yeah!");
            Assert.IsNull(command);
        }
    }
}
