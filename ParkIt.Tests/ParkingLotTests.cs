﻿using System;
using Boromak.Snippets.Testing.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkIt.Commands;

namespace ParkIt.Tests
{
    [TestClass]
    public class ParkingLotTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Parking lot color cannot be ALL")]
        public void ParkingLotALLFail()
        {
            ParkingLot pl = new ParkingLot(ParkingLot.ParkingLotColors.ALL, 20, 2);   
        }

       
    }
}
