﻿using System;
using System.Text;
using System.Collections.Generic;
using Boromak.Snippets.Testing.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkIt.Commands;

namespace ParkIt.Tests
{

    [TestClass]
    public class CommandTests
    {
        

        [TestMethod]
        public void DisplayCommandDisplaysPlacesLeft()
        {
            ParkingManager pk = new ParkingManager();
            DisplayCommand dc = new DisplayCommand(pk, "RED");
            using (var consoleOut = new ConsoleOutput())
            {
                dc.ExecuteCommand();
                Assert.AreEqual("Parking lot RED has 20 places left.\r\n", consoleOut.GetOuput());
            }
        }

        

        [TestMethod]
        public void AddCommandAddsCar()
        {
            ParkingManager pk = new ParkingManager();
            AddCommand dc = new AddCommand(pk, "9379992", "RED");
            using (var consoleOut = new ConsoleOutput())
            {
                dc.ExecuteCommand();
                Assert.AreEqual("Car 9379992 parked at R0.\r\n", consoleOut.GetOuput());
            }
        }

        [TestMethod]
        public void AddCommandFailsOnBigCarNumber()
        {
            ParkingManager pk = new ParkingManager();
            AddCommand dc = new AddCommand(pk, "93799921111", "RED");
            dc.ExecuteCommand();
            Assert.AreEqual("Parameters did not pass validation.", dc.LastErrorMessage);
        }

        [TestMethod]
        public void SearchCommandDisplaysCar()
        {
            ParkingManager pk = new ParkingManager();
            pk.ParkNewCar("9379992", "RED");
            SearchCommand sc = new SearchCommand(pk, "9379992");
            using (var consoleOut = new ConsoleOutput())
            {
                sc.ExecuteCommand();
                Assert.AreEqual("Car 9379992 is at R0 (120 minutes left).\r\n", consoleOut.GetOuput());
            }
        }
        [TestMethod]
        public void SearchCommandUnexistingCar()
        {
            ParkingManager pk = new ParkingManager();
            SearchCommand sc = new SearchCommand(pk, "9379992");
            using (var consoleOut = new ConsoleOutput())
            {
                sc.ExecuteCommand();
                Assert.AreEqual("Car 9379992 not found.\r\n", consoleOut.GetOuput());
            }
        }

        [TestMethod]
        public void ExpiredCarsReturnsEmpty()
        {
            ParkingManager pk = new ParkingManager();
            ExpiredCommand ec = new ExpiredCommand(pk);
            using (var consoleOut = new ConsoleOutput())
            {
                ec.ExecuteCommand();
                Assert.AreEqual(
                        "No expired cars in parking lot RED.\r\n"+
                        "No expired cars in parking lot YELLOW.\r\n"+
                        "No expired cars in parking lot GREEN.\r\n", 
                    consoleOut.GetOuput());
            }
        }

        [TestMethod]
        public void ExpiredCommandOneInEachLot()
        {
            ParkingManager pk = new ParkingManager();
            var badParkingTime = DateTime.MinValue.AddMinutes(1);
            pk.ParkNewCar("123111", "RED", badParkingTime);
            pk.ParkNewCar("123112", "YELLOW", badParkingTime);
            pk.ParkNewCar("123113", "GREEN", badParkingTime);
            ExpiredCommand ec = new ExpiredCommand(pk);
            using (var consoleOut = new ConsoleOutput())
            {
                ec.ExecuteCommand();
                Assert.AreEqual(
                        "Expired cars at lot RED\r\n" +
                        " - 123111\r\n" +
                         "Expired cars at lot YELLOW\r\n" +
                        " - 123112\r\n" +
                         "Expired cars at lot GREEN\r\n" +
                        " - 123113\r\n" ,
                    consoleOut.GetOuput());
            }
        }

        [TestMethod]
        public void ResetCommandResetsCar()
        {
            ParkingManager pk = new ParkingManager();
            var badParkingTime = DateTime.MinValue.AddMinutes(1);
            pk.ParkNewCar("123111", "RED", badParkingTime);
            ResetCommand rc = new ResetCommand(pk, "123111");
            using (var consoleOut = new ConsoleOutput())
            {
                rc.ExecuteCommand();
                Assert.AreEqual("Parking time for car 123111 has been reset.\r\n",
                    consoleOut.GetOuput());
            }
        }

        [TestMethod]
        public void CountdownCommandDecreasesTime()
        {
            ParkingManager pk = new ParkingManager();
            pk.ParkNewCar("9379992", "RED");
            CountdownCommand cc = new CountdownCommand(pk, "admin", "password");
            SearchCommand sc = new SearchCommand(pk, "9379992");
            cc.ExecuteCommand();
            using (var consoleOut = new ConsoleOutput())
            {
                
                sc.ExecuteCommand();
                Assert.AreEqual("Car 9379992 is at R0 (115 minutes left).\r\n", consoleOut.GetOuput());
            }
        }

    }
}
