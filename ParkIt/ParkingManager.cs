﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ParkIt
{
    /// <summary>
    ///     Parking manager contains all the parking lots and is used to interact with them
    /// </summary>
    public class ParkingManager
    {
        private readonly List<ParkingLot> _parkingLots = new List<ParkingLot>
        {
            new ParkingLot(ParkingLot.ParkingLotColors.RED, 20, 2),
            new ParkingLot(ParkingLot.ParkingLotColors.YELLOW, 15, 1),
            new ParkingLot(ParkingLot.ParkingLotColors.GREEN, 10, 0.5)
        };

        /// <summary>
        ///     Prints the place availability in the specified parking lot
        ///     <para> If ANY is passed, it will display the info for all lots</para>
        /// </summary>
        /// <param name="parkingLotColor">the parking lot to display the availability for</param>
        public void DisplayParkingLot(string parkingLotColor)
        {
            ParkingLot.ParkingLotColors color;
            if (!Enum.TryParse(parkingLotColor, true, out color))
            {
                Console.WriteLine("Display command failed. Parking lot color could not be parsed.");
                return;
            }

            _parkingLots.Where(o => o.Color == color || color == ParkingLot.ParkingLotColors.ALL)
                .ToList().ForEach(o => o.PrintPlacesLeft());
        }

        private ParkingLot GetParkingLotContainingCar(string carId)
        {
            ParkingLot containsCar = null;
            _parkingLots.ForEach(o =>
            {
                if (o.ContainsCar(carId))
                {
                    containsCar = o;
                }
            });
            return containsCar;
        }


        /// <summary>
        ///     Adds a new car to the specified parking lot
        /// </summary>
        /// <param name="carId">number of the car to add</param>
        /// <param name="lotColor">name of the parking lot to park in</param>
        /// <param name="parkedAt">at what time was the car parked. by default it's the current time</param>
        public void ParkNewCar(string carId, string lotColor, DateTime parkedAt = default(DateTime))
        {

            var carFound = GetParkingLotContainingCar(carId);
            if (carFound != null)
            {
                Console.WriteLine("Car {0} was not added. A car with that ID is already parked.", carId);
                return;
            }
            if (parkedAt == default(DateTime)) parkedAt = DateTime.Now;
            
            var color = (ParkingLot.ParkingLotColors) Enum.Parse(typeof (ParkingLot.ParkingLotColors), lotColor);
            var lot = _parkingLots.Find(o => o.Color == color);
            if (!lot.HasAvailableSpot())
            {
                Console.WriteLine("Could not park {0} at {1}. No spot available", carId, lotColor);
            }
            lot.ParkCar(carId, parkedAt);
        }

        /// <summary>
        ///     Prints the lot and place where the car is parked and the time left
        /// </summary>
        /// <param name="carId">id of the car to check the status for</param>
        public void DisplayCarSpot(string carId)
        {
            var carFound = GetParkingLotContainingCar(carId);
            if (carFound == null)
            {
                Console.WriteLine("Car {0} not found.", carId);
                return;
            }
            carFound.PrintCarStatus(carId);
        }

        /// <summary>
        ///     Prints all the expired cars accross all parking lots
        /// </summary>
        public void DisplayExpiredCars()
        {
            _parkingLots.ForEach(o => { o.DisplayExpiredCars(); });
        }

        /// <summary>
        ///     Resets the parking time for a specific car
        /// </summary>
        /// <param name="carId"></param>
        public void ResetCarTime(string carId)
        {
            var carFound = GetParkingLotContainingCar(carId);
            if (carFound == null)
            {
                Console.WriteLine("Car {0} not found.", carId);
                return;
            }
            carFound.ResetParkingTime(carId);
        }

        /// <summary>
        ///     Tries to authenthicate the user in the system
        /// </summary>
        /// <param name="login">login to the system</param>
        /// <param name="password">password to the system</param>
        /// <returns></returns>
        private bool Authenthicate(string login, string password)
        {
            if (login == "admin" && password == "password")
                return true;
            Console.WriteLine("Access denied. Invalid login credentials.");
            return false;
        }

        /// <summary>
        ///     Deducts five minutes from all parked cars. Authethication required
        /// </summary>
        /// <param name="login">login to the system</param>
        /// <param name="password">password to the system</param>
        public void ForceCountdown(string login, string password)
        {
            if (!Authenthicate(login, password)) return;
            _parkingLots.ForEach(o => { o.ForceCountdown(); });
            Console.WriteLine("Success. 5 minutes substracted from all cars");
        }
    }
}