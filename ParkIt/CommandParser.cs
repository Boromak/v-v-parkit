﻿using System;
using System.Linq;
using ParkIt.Commands;

namespace ParkIt
{
    public class CommandParser
    {
        /// <summary>
        ///     Parses strings into commands and arguments to ParkingCommand objects.
        ///     <para> if string cannot be parsed returns null</para>
        /// </summary>
        /// <param name="parkingManager"></param>
        /// <param name="line"></param>
        /// <returns>returns the corresponding command object</returns>
        public static ParkingCommand ParseLineToCommand(ParkingManager parkingManager, string line)
        {
            if (string.IsNullOrWhiteSpace(line)) return null;
            var words = line.Split(' ');
            if (words.Length < 1) return null;
            var parameters = words.Length < 2 ? new string[0] : words.Skip(1).ToArray();

            switch (words[0])
            {
                case "help":
                    return new HelpCommand(parkingManager, parameters);
                case "display":
                    return new DisplayCommand(parkingManager, parameters);
                    
                case "add":
                    return new AddCommand(parkingManager, parameters);
                    
                case "search":
                    return new SearchCommand(parkingManager, parameters);
                    
                case "expired":
                    return new ExpiredCommand(parkingManager, parameters);
                    
                case "reset":
                    return new ResetCommand(parkingManager, parameters);
                    
                case "countdown":
                    return new CountdownCommand(parkingManager, parameters);
                    
            }
            return null;
        }

        /// <summary>
        ///     Parses and executes a list of commands
        /// </summary>
        /// <param name="lines">list of commands</param>
        /// <param name="showCommands">should the commands that are parsed be displayed</param>
        public static void HandleCommands(string[] lines, bool showCommands)
        {
            var pk = new ParkingManager();
            foreach (var line in lines)
            {
                if (showCommands)
                    Console.WriteLine("- Parsing command [{0}]", line);
                var command = ParseLineToCommand(pk, line);
                if (command == null)
                {
                    Console.WriteLine("Could not parse [{0}]", line);
                    continue;
                }
                command.ExecuteCommand();
                if (!string.IsNullOrWhiteSpace(command.LastErrorMessage))
                    Console.WriteLine("Command [{0}] failed. Reason: {1}", line, command.LastErrorMessage);
            }
        }
    }
}