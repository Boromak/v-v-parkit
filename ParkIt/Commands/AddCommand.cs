﻿using Boromak.Snippets.Structures.Helpers;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Represents a parsed ADD command. Accepts two arguments,:
    ///     <para />
    ///     - first one should be from 5 to 7 digits
    ///     <para />
    ///     - second one should be RED, YELLOW or GREEN
    ///     <para />
    ///     If parameters do not match the regular expressions they will not be validated.
    /// </summary>
    public class AddCommand : ParkingCommand
    {
        public AddCommand(ParkingManager parkMan, params string[] args) : base(parkMan, args)
        {
        }

        protected override Range<int> ParameterAmountRange => new Range<int> {Minimum = 2, Maximum = 2};
        protected override string[] ParameterRegexPattern => new[] {@"^\d{5,7}$", "RED|YELLOW|GREEN"};

        protected override bool InternalExecuteCommand()
        {
            ParkMan.ParkNewCar(_parameters[0], _parameters[1]);

            return true;
        }
    }
}