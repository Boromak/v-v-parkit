﻿using System;
using Boromak.Snippets.Structures.Helpers;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Represents a parsed HELP command. Does not accept arguments.
    /// </summary>
    public class HelpCommand : ParkingCommand
    {
        public HelpCommand(ParkingManager parkMan, params string[] args) : base(parkMan, args)
        {
        }

        protected override Range<int> ParameterAmountRange => new Range<int> {Minimum = 0, Maximum = 0};
        protected override string[] ParameterRegexPattern => new string[0];

        protected override bool InternalExecuteCommand()
        {
            //TODO: Make less dirty.. someday.
            Console.WriteLine(
                "Printing out the help menu\r\n" +
                " help - displays this text.\n\n" +
                " display <ARGUMENT> - displays the number of parking spaces in the selected parking lot. Acceptable arguments are: ALL, RED, YELLOW, GREEN.\n\n" +
                " add <ARGUMENT1> <ARGUMENT2> - parks a car in a specific zone and will be given the first  free space available  from the chosen floor which is in the following format :a letter  that indicates the floor and a number that indicates the space where the car is  parked in that floor. Acceptable arguments for ARGUMENT1 are: combination of 5-7 alphanumeric  characters. Acceptable arguments for ARGUMENT2 are: RED , YELLOW, GREEN.\n\n" +
                " search <ARGUMENT> - displays  where the car is parked(letter of the floor and number of the spot) and the remaining time for the parking.\n\n" +
                " expired - displays of all the cars that have 0 or less minutes to stay.\n\n" +
                " reset <ARGUMENT> - resets the time for a car. Acceptable arguments are: number of the car without any spaces.\n\n" +
                " countdown - This function is available only for administrators. A login is needed. Decrements the parking time of all cars by five minutes.\n\n");
            return true;
        }
    }
}