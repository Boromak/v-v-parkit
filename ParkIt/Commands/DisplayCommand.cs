﻿using Boromak.Snippets.Structures.Helpers;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Represents a parsed DISPLAY command. Accepts one argument:
    ///     <para />
    ///     - a parking lot identifier (ALL/RED/YELLOW/GREEN)
    ///     <para />
    ///     If parameters do not match the regular expressions they will not be validated.
    /// </summary>
    public class DisplayCommand : ParkingCommand
    {
        public DisplayCommand(ParkingManager parkMan, params string[] args) : base(parkMan, args)
        {
        }

        protected override Range<int> ParameterAmountRange => new Range<int> {Minimum = 1, Maximum = 1};
        protected override string[] ParameterRegexPattern => new[] {"^ALL|RED|YELLOW|GREEN$"};

        protected override bool InternalExecuteCommand()
        {
            ParkMan.DisplayParkingLot(_parameters[0]);
            return true;
        }
    }
}