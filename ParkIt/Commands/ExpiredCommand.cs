﻿using Boromak.Snippets.Structures.Helpers;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Represents a parsed ADD command. Does not need any arguments
    /// </summary>
    public class ExpiredCommand : ParkingCommand
    {
        public ExpiredCommand(ParkingManager parkMan, params string[] args) : base(parkMan, args)
        {
        }

        protected override Range<int> ParameterAmountRange => new Range<int> {Minimum = 0, Maximum = 0};
        protected override string[] ParameterRegexPattern => new string[0];

        protected override bool InternalExecuteCommand()
        {
            ParkMan.DisplayExpiredCars();
            return true;
        }
    }
}