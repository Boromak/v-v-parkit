﻿using Boromak.Snippets.Structures.Helpers;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Represents a parsed SEARCH command. Accepts one argument:
    ///     <para />
    ///     - should be from 5 to 7 digits
    ///     <para />
    ///     If the argument do not
    public class SearchCommand : ParkingCommand
    {
        public SearchCommand(ParkingManager parkMan, params string[] args) : base(parkMan, args)
        {
        }

        protected override Range<int> ParameterAmountRange => new Range<int> {Minimum = 1, Maximum = 1};
        protected override string[] ParameterRegexPattern => new[] {@"^\d{5,7}$"};

        protected override bool InternalExecuteCommand()
        {
            ParkMan.DisplayCarSpot(_parameters[0]);
            return true;
        }
    }
}