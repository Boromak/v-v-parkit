﻿using Boromak.Snippets.Structures.Helpers;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Represents a parsed COUNTDOWN command. Accepts two arguments:
    ///     <para />
    ///     - admin username
    ///     <para />
    ///     - admin password
    ///     <para />
    /// </summary>
    public class CountdownCommand : ParkingCommand
    {
        public CountdownCommand(ParkingManager parkMan, params string[] args) : base(parkMan, args)
        {
        }

        protected override Range<int> ParameterAmountRange => new Range<int> {Minimum = 2, Maximum = 2};
        protected override string[] ParameterRegexPattern => new string[0];

        protected override bool InternalExecuteCommand()
        {
            ParkMan.ForceCountdown(_parameters[0], _parameters[1]);
            return true;
        }
    }
}