﻿using Boromak.Snippets.Structures.Commands;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Base parking command. References the parking manager object, which is basically a context for commands
    /// </summary>
    public abstract class ParkingCommand : BaseStringCommand
    {
        protected ParkingManager ParkMan;

        public ParkingCommand(ParkingManager parkMan, params string[] args) : base(args)
        {
            ParkMan = parkMan;
        }
    }
}