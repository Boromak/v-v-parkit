﻿using Boromak.Snippets.Structures.Helpers;

namespace ParkIt.Commands
{
    /// <summary>
    ///     Represents a parsed RESET command. Accepts one argument:
    ///     <para />
    ///     - should be from 5 to 7 digits
    ///     <para />
    ///     If the argument do not match the regular expression it will not be validated.
    /// </summary>
    public class ResetCommand : ParkingCommand
    {
        public ResetCommand(ParkingManager parkMan, params string[] args) : base(parkMan, args)
        {
        }

        protected override Range<int> ParameterAmountRange => new Range<int> {Minimum = 1, Maximum = 1};
        protected override string[] ParameterRegexPattern => new[] {@"^\d{5,7}$"};

        protected override bool InternalExecuteCommand()
        {
            ParkMan.ResetCarTime(_parameters[0]);
            return true;
        }
    }
}