﻿using System;
using System.IO;

namespace ParkIt
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Check that a parameter is passed and that file actually exists
            //If any of this fails - print a message and exit
            if (args.Length <= 0)
            {
                Console.WriteLine("An input file must be specified.");
                return;
            }
            if (!File.Exists(args[0]))
            {
                Console.WriteLine("Could not find file {0}", args[0]);
                return;
            }
            var showCommands = args.Length >= 2 && args[1].ToLower() == "--showcommands";
            var lines = File.ReadAllLines(args[0]);

            CommandParser.HandleCommands(lines, showCommands);

            Console.WriteLine("Press ENTER key to exit.");
            Console.ReadLine();
        }
    }
}