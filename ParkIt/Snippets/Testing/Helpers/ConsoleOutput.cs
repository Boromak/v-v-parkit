﻿using System;
using System.IO;

namespace Boromak.Snippets.Testing.Helpers
{
    /// <summary>
    /// Allows to redirect the console output to a string
    /// </summary>
    public class ConsoleOutput : IDisposable
    {
        private readonly TextWriter _originalOutput;
        private readonly StringWriter _stringWriter;

        public ConsoleOutput()
        {
            _stringWriter = new StringWriter();
            _originalOutput = Console.Out;
            Console.SetOut(_stringWriter);
        }

        public void Dispose()
        {
            Console.SetOut(_originalOutput);
            _stringWriter.Dispose();
        }

        public string GetOuput()
        {
            return _stringWriter.ToString();
        }
    }
}