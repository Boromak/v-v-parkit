﻿using System.Text.RegularExpressions;
using Boromak.Snippets.Structures.Helpers;

namespace Boromak.Snippets.Structures.Commands
{
    /// <summary>
    ///     Base class for string commands. Has a specified range of parameters and checks by regular expressions if the
    ///     parameters are valid
    /// </summary>
    public abstract class BaseStringCommand
    {
        protected readonly string[] _parameters;

        protected BaseStringCommand(params string[] args)
        {
            _parameters = args;
        }

        public string LastErrorMessage { get; protected set; }

        /// <summary>
        ///     Amount of parameters permitted. Has to be overriden
        /// </summary>
        protected abstract Range<int> ParameterAmountRange { get; }

        /// <summary>
        ///     Regular expressions for parameters
        /// </summary>
        protected abstract string[] ParameterRegexPattern { get; }

        /// <summary>
        ///     Executes the parsed command
        /// </summary>
        /// <returns>if the command execution was successful </returns>
        public bool ExecuteCommand()
        {
            if (AreParametersValid()) return InternalExecuteCommand();
            LastErrorMessage = "Parameters did not pass validation.";
            return false;
        }

        /// <summary>
        ///     If not overriden, checks that the amount of parameters match ParameterRange and the commands itself match the
        ///     RegexPatterns
        /// </summary>
        /// <returns> if the parameters are valid</returns>
        protected virtual bool AreParametersValid()
        {
            if (!ParameterAmountRange.ContainsValue(_parameters.Length)) return false;
            for (var i = 0; i < _parameters.Length; i++)
            {
                if (i >= ParameterRegexPattern.Length) break;
                if (!Regex.Match(_parameters[i], ParameterRegexPattern[i]).Success) return false;
            }
            return true;
        }

        /// <summary>
        ///     The actual execution of the command. Has to be overriden in child classes
        /// </summary>
        /// <returns></returns>
        protected abstract bool InternalExecuteCommand();
    }
}