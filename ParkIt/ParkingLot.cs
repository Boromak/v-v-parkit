﻿using System;
using System.Linq;

namespace ParkIt
{
    /// <summary>
    ///     Represents a parking lot containing the parked cars and functions interact with them
    /// </summary>
    public class ParkingLot
    {
        /// <summary>
        ///     Possible parking lot colors
        /// </summary>
        public enum ParkingLotColors
        {
            ALL,
            RED,
            YELLOW,
            GREEN
        }

        private readonly ParkedCar[] _parkedCars;

        /// <summary>
        ///     Default constructor for parking lots
        /// </summary>
        /// <param name="parkingLotColor">will throw an exception if value is ALL</param>
        /// <param name="size">size of the parking lot</param>
        /// <param name="hours">allowed parking time</param>
        public ParkingLot(ParkingLotColors parkingLotColor, int size, double hours)
        {
            if (parkingLotColor == ParkingLotColors.ALL) throw new ArgumentException("Parking lot color cannot be ALL");
            if (size <= 0) throw new ArgumentException("Size of the parking lot cannot be zero");
            if (hours <= 0) throw new ArgumentException("Parking time cannot be zero");

            Color = parkingLotColor;
            Size = size;
            Hours = hours;
            _parkedCars = new ParkedCar[Size];
        }

        public ParkingLotColors Color { get; }
        public int Size { get; }
        public double Hours { get; }

        /// <summary>
        ///     Prints the places left in this parking lot
        /// </summary>
        public void PrintPlacesLeft()
        {
            Console.WriteLine("Parking lot {0} has {1} places left.", Color, (Size - _parkedCars.Count(o => o != null)));
        }

        /// <summary>
        ///     Checks if this parking lot has available places
        /// </summary>
        /// <returns>place availability</returns>
        public bool HasAvailableSpot()
        {
            return Size - _parkedCars.Count(o => o != null) > 0;
        }

        /// <summary>
        ///     Parks a car with the car number at the desired time
        /// </summary>
        /// <param name="carNumber">car number</param>
        /// <param name="parkedAt">time to park the car at</param>
        /// <returns></returns>
        public bool ParkCar(string carNumber, DateTime parkedAt)
        {
            if (!HasAvailableSpot()) return false;
            for (var index = 0; index < _parkedCars.Length; index++)
            {
                if (_parkedCars[index] != null) continue;

                _parkedCars[index] = new ParkedCar {Id = carNumber, ParkedAt = parkedAt};
                Console.WriteLine("Car {0} parked at {1}{2}.", carNumber, Color.ToString().Substring(0, 1), index);
                return true;
            }
            Console.WriteLine("Could'nt find a parking spot at {0}", Color);
            return false;
        }

        /// <summary>
        ///     Checks if this parking lot contains the specified car
        /// </summary>
        /// <param name="carId"></param>
        /// <returns></returns>
        public bool ContainsCar(string carId)
        {
            return _parkedCars.Where(parkedCar => parkedCar != null)
                .Any(parkedCar => parkedCar.Id == carId);
        }

        /// <summary>
        ///     Prints the status for a specific car
        /// </summary>
        /// <param name="carId">car number</param>
        public void PrintCarStatus(string carId)
        {
            var parkedCar = _parkedCars.Where(o => o != null).FirstOrDefault(o => o.Id == carId);
            if (parkedCar == null) return;
            var index = Array.IndexOf(_parkedCars, parkedCar);
            var minutesLeft = (parkedCar.ParkedAt.AddHours(Hours) - DateTime.Now).TotalMinutes;
            Console.WriteLine("Car {0} is at {1}{2} ({3}).", carId, Color.ToString().Substring(0, 1),
                index, minutesLeft > 0 ? minutesLeft.ToString("0") + " minutes left" : "expired");
        }

        /// <summary>
        ///     Prints out cars that have their parking time expired
        /// </summary>
        public void DisplayExpiredCars()
        {
            var expiredCars = _parkedCars.Where(o => o != null)
                .Where(o => (o.ParkedAt.AddHours(Hours) - DateTime.Now).TotalMinutes <= 0).ToList();
            if (expiredCars.Count == 0)
            {
                Console.WriteLine("No expired cars in parking lot {0}.", Color);
                return;
            }
            Console.WriteLine("Expired cars at lot {0}", Color);
            expiredCars.ForEach(o => Console.WriteLine(" - {0}", o.Id));
        }

        /// <summary>
        ///     Resets the specified car parking time
        /// </summary>
        /// <param name="carId"></param>
        public void ResetParkingTime(string carId)
        {
            var parkedCar = _parkedCars.Where(o => o != null).FirstOrDefault(o => o.Id == carId);
            if (parkedCar == null) return;
            parkedCar.ParkedAt = DateTime.Now;
            Console.WriteLine("Parking time for car {0} has been reset.", carId);
        }

        /// <summary>
        ///     Deducts 5 minutes from the parking time of all cars
        /// </summary>
        public void ForceCountdown()
        {
            foreach (var parkedCar in _parkedCars)
            {
                if (parkedCar == null) continue;
                parkedCar.ParkedAt = parkedCar.ParkedAt.AddMinutes(-5);
            }
        }

        /// <summary>
        ///     Represents the parked cars
        /// </summary>
        private class ParkedCar
        {
            public string Id { get; set; }

            public DateTime ParkedAt { get; set; }
        }
    }
}